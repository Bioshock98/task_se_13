package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.Session;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskListAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        if(bootstrap.getSession() == null) return false;
        return bootstrap.getSession().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "task_list_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "list all users tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST ADMIN]");
        System.out.println("CHOOSE SORT TYPE");
        System.out.println("[createTime, startDate, endDate, status]");
        @NotNull final String option = bootstrap.getTerminalService().nextLine();
        @NotNull final ITaskEndpoint taskEndpoint = bootstrap.getTaskEndpoint();
        @NotNull final Session session = bootstrap.getSession();
        switch (option) {
            case "createTime" :
                printTasks(taskEndpoint.sortTasksByCreateTime(bootstrap.getSession(), session.getUserId(), 1));
                break;
            case "startDate" :
                printTasks(taskEndpoint.sortTasksByStartDate(bootstrap.getSession(), session.getUserId(), 1));
                break;
            case "finishDate" :
                printTasks(taskEndpoint.sortTasksByFinishDate(bootstrap.getSession(), session.getUserId(), 1));
                break;
            case "status" :
                printTasks(taskEndpoint.sortTasksByStatus(bootstrap.getSession(), session.getUserId(), 1));
        }
        System.out.println("[OK]");
    }
}
