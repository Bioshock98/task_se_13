package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.*;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.lang.Exception;
import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskListByProjectAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        if(bootstrap.getSession() == null) return false;
        return bootstrap.getSession().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "task_list_by_project_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "list tasks by any user project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        @NotNull final ITaskEndpoint taskEndpoint = bootstrap.getTaskEndpoint();
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("[ENTER PROJECT ID");
        @NotNull final List<Project> projects = projectEndpoint.findAllProjects(bootstrap.getSession());
        printProjects(projects);
        final int projectNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String projectId = projects.get(projectNumber).getId();
        @NotNull final List<Task> tasksByProjectId = taskEndpoint.findTasksByProjectId(bootstrap.getSession(), projectId);
        printTasks(tasksByProjectId);
        System.out.println("[OK]");
    }
}
