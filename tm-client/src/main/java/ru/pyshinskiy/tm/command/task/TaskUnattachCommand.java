package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Task;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskUnattachCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_unattach";
    }

    @Override
    @NotNull
    public String description() {
        return "unattach task from project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITaskEndpoint taskEndpoint = bootstrap.getTaskEndpoint();
        System.out.println("[UNATTACH TASK]");
        @NotNull final List<Task> tasks = taskEndpoint.findAllTasksByUserId(bootstrap.getSession());
        printTasks(tasks);
        final int taskNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String taskId = tasks.get(taskNumber).getId();
        @Nullable final Task task = taskEndpoint.findOneTaskByUserId(bootstrap.getSession(), taskId);
        if(task == null) {
            throw new Exception("task doesn't exist");
        }
        task.setProjectId(null);
        taskEndpoint.mergeTask(bootstrap.getSession(), task);
        System.out.println("[OK]");
    }
}
