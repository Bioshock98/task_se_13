package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.User;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class UserChangePasswordCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "user_change_password";
    }

    @Override
    @NotNull
    public String description() {
        return "change user password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CHANGE PASSWORD");
        System.out.println("ENTER NEW PASSWORD");
        @NotNull final User user = new User();
        user.setId(bootstrap.getSession().getUserId());
        user.setPasswordHash(terminalService.nextLine());
        bootstrap.getUserEndpoint().mergeUser(bootstrap.getSession(), user);
        System.out.println("[OK]");
    }
}
