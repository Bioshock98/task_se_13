package ru.pyshinskiy.tm.api.user;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    @NotNull
    @Select("SELECT * FROM `app_user`")
    List<User> findAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM `app_user` WHERE `id` = #{id}")
    User findOne(@NotNull final String id) throws Exception;

    @Insert("INSERT INTO `app_user` (`id`, `login`, `passwordHash`, `role`)" +
            " VALUES(#{id}, #{login}, #{passwordHash}, #{role})")
    void persist(@NotNull final User user) throws Exception;

    @Update("UPDATE `app_user` SET `login` = #{login}, `passwordHash` = #{passwordHash}" +
            "WHERE `id` = #{id}")
    void merge(@NotNull final User user) throws Exception;

    @Delete("DELETE FROM `app_user` WHERE `id` = #{id}")
    void remove(@NotNull final String id) throws Exception;

    @Delete("DELETE FROM `app_user`")
    void removeAll() throws Exception;

    @Select("SELECT * FROM `app_user` WHERE `login` = #{login}")
    @Nullable
    User getUserByLogin(@NotNull final String login) throws Exception;
}
