package ru.pyshinskiy.tm.api.session;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Session;

import java.util.List;

public interface ISessionRepository {

    @NotNull
    @Select("SELECT * FROM `app_session`")
    List<Session> findAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM `app_session` WHERE `id`= #{id}")
    Session findOne(@NotNull final String id) throws Exception;

    @Insert("INSERT INTO `app_session` (`id`, `userId`, `role`, `timestamp`, `signature`) " +
            "VALUES(#{id}, #{userId}, #{role}, #{timestamp}, #{signature})")
    void persist(@NotNull final Session session) throws Exception;

    @Update("UPDATE `app_session` SET `userId` = #{userId}, `role` = #{role}, `timestamp` = #{timastamp}," +
            "`signature = #{signature}")
    void merge(@NotNull final Session session) throws Exception;

    @Delete("DELETE FROM `app_session` WHERE `id` = #{id}")
    void remove(@NotNull final String id) throws Exception;

    @Delete("DELETE FROM `app_session`")
    void removeAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM `app_session` WHERE `id` = #{id} and `userId` = #{userId}")
    Session findOneByUserId(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id) throws Exception;

    @Delete("DELETE FROM `app_session` WHERE `userId` = #{userId} and `id` = #{id}")
    void removeByUserId(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id) throws Exception;
}
