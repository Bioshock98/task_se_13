package ru.pyshinskiy.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @WebMethod
    @Nullable
    User findOneUser(@Nullable final Session session, @Nullable final String id) throws Exception;

    @WebMethod
    @NotNull
    List<User> findAllUsers(@Nullable final Session session) throws Exception;

    @WebMethod
    void persistUser(@NotNull final String login, @NotNull final String password, @NotNull final Role role) throws Exception;

    @WebMethod
    void mergeUser(@Nullable final Session session, @Nullable final User user) throws Exception;

    @WebMethod
    void removeUser(@Nullable final Session session, @Nullable final String id) throws Exception;

    @WebMethod
    void removeAllUsers(@Nullable final Session session) throws Exception;

    @WebMethod
    @Nullable
    User getUserByLoginE(@Nullable final Session session, @Nullable final String login) throws Exception;
}
