package ru.pyshinskiy.tm.api.project;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Project;

import java.util.List;

public interface
IProjectRepository {

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `userId` = #{userId}")
    List<Project> findAllByUserId(@NotNull final String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_project`")
    List<Project> findAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM `app_project` WHERE `id` = #{id}")
    Project findOne(@NotNull final String id) throws Exception;

    @Insert("INSERT INTO `app_project` (`id`, `userId`, `createTime`, `name`, `description`, `status`, `startDate`, `finishDate`)" +
            "VALUES (#{id}, #{userId}, #{createTime}, #{name}, #{description}, #{status}, #{startDate}, #{finishDate})")
    void persist(@NotNull final Project project) throws Exception;

    @Update("UPDATE `app_project` SET `userId`= #{userId}, `createTime`= #{createTime}, `name`= #{name}, `description`= #{description}," +
            "`status`= #{status}, `startDate`= #{startDate}, `finishDate`= #{finishDate} WHERE `id`= #{id}")
    void merge(@NotNull final Project project) throws Exception;

    @Delete("DELETE FROM `app_project` WHERE `id`= #{id}")
    void remove(@NotNull final String id) throws Exception;

    @Delete("DELETE FROM `app_project`")
    void removeAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM `app_project` WHERE `userId`= #{userId} and `id` = #{id}")
    Project findOneByUserId(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `userId`= #{userId} and `name` = #{name}")
    List<Project> findByName(@NotNull @Param("userId") final String userId, @NotNull @Param("name") final String name) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `userId`= #{userId} and `description` = #{description}")
    List<Project> findByDescription(@NotNull @Param("userId") final String userId, @NotNull @Param("description") final String description) throws Exception;

    @Delete("DELETE FROM `app_project` WHERE `id`= #{id} and `userId`= #{userId}")
    void removeByUserId(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id) throws Exception;

    @Delete("DELETE FROM `app_project` WHERE `userId`= #{userId}")
    void removeAllByUserId(@NotNull final String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `userId`= #{userId} ORDER BY `createTime`")
    List<Project> sortByCreateTime(@NotNull final @Param("userId") String userId, @Param("direction") final int direction) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `userId`= #{userId} ORDER BY `startDate`")
    List<Project> sortByStartDate(@NotNull final @Param("userId") String userId, @Param("direction") final int direction) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `userId`= #{userId} ORDER BY `finishDate`")
    List<Project> sortByFinishDate(@NotNull final @Param("userId") String userId, @Param("direction") final int direction) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `userId`= #{userId} ORDER BY FIELD(status, 'PLANNED', 'IN_PROGRESS', 'DONE')")
    List<Project> sortByStatus(@NotNull final @Param("userId") String userId, @Param("direction") final int direction) throws Exception;
}
