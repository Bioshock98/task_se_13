package ru.pyshinskiy.tm.api.task;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `userId` = #{userId}")
    List<Task> findAllByUserId(@NotNull final String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_task`")
    List<Task> findAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM `app_task` WHERE `id` = #{id}")
    Task findOne(@NotNull final String id) throws Exception;

    @Insert("INSERT INTO `app_task` (`id`, `userId`, `projectId`, `createTime`, `name`, `description`, `status`, `startDate`, `finishDate`) " +
            "VALUES (#{id}, #{userId}, #{projectId}, #{createTime}, #{name}, #{description}, #{status}, #{startDate}, #{finishDate})")
    void persist(@NotNull final Task task) throws Exception;

    @Update("UPDATE `app_task` SET `userId`= #{userId}, `projectId`= #{projectId}, `createTime`= #{createTime}, `name`= #{name}, `description`= #{description}," +
            "`status`= #{status}, `startDate`= #{startDate}, `finishDate`= #{finishDate} WHERE `id`= #{id}")
    void merge(@NotNull final Task task) throws Exception;

    @Delete("DELETE FROM `app_task` WHERE `id`= #{id}")
    void remove(@NotNull final String id) throws Exception;

    @Delete("DELETE FROM `app_task`")
    void removeAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM `app_task` WHERE `userId`= #{userId} and `id` = #{id}")
    Task findOneByUserId(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `userId`= #{userId} and `name` = #{name}")
    List<Task> findByName(@NotNull @Param("userId") final String userId, @NotNull @Param("name") final String name) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `userId`= #{userId} and `description` = #{description}")
    List<Task> findByDescription(@NotNull @Param("userId") final String userId, @NotNull @Param("description") final String description) throws Exception;

    @Delete("DELETE FROM `app_task` WHERE `id`= #{id} and `userId`= #{userId}")
    void removeByUserId(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id) throws Exception;

    @Delete("DELETE FROM `app_task` WHERE `userId`= #{userId}")
    void removeAllByUserId(@NotNull final String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `userId`= #{userId} ORDER BY `createTime`")
    List<Task> sortByCreateTime(@NotNull @Param("userId") final String userId, @Param("direction") final int direction) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `userId`= #{userId} ORDER BY `startDate`")
    List<Task> sortByStartDate(@NotNull @Param("userId") final String userId, @Param("direction") final int direction) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `userId`= #{userId} ORDER BY `finishDate`")
    List<Task> sortByFinishDate(@NotNull @Param("userId") final String userId, @Param("direction") final int direction) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `userId`= #{userId} ORDER BY FIELD(status, 'PLANNED', 'IN_PROGRESS', 'DONE')")
    List<Task> sortByStatus(@NotNull @Param("userId") final String userId, @Param("direction") final int direction) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `userId`= #{userId} and `projectId`= #{projectId}")
    List<Task> findAllByProjectId(@NotNull @Param("userId") final String userId, @NotNull @Param("projectId") final String projectId) throws Exception;
}
