package ru.pyshinskiy.tm.bootstrap;

import lombok.NoArgsConstructor;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.project.IProjectRepository;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.session.ISessionRepository;
import ru.pyshinskiy.tm.api.session.ISessionService;
import ru.pyshinskiy.tm.api.task.ITaskRepository;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.api.user.IUserRepository;
import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.endpoint.ProjectEndpoint;
import ru.pyshinskiy.tm.endpoint.SessionEndpoint;
import ru.pyshinskiy.tm.endpoint.TaskEndpoint;
import ru.pyshinskiy.tm.endpoint.UserEndpoint;
import ru.pyshinskiy.tm.service.ProjectService;
import ru.pyshinskiy.tm.service.SessionService;
import ru.pyshinskiy.tm.service.TaskService;
import ru.pyshinskiy.tm.service.UserService;

import javax.sql.DataSource;
import javax.xml.ws.Endpoint;
import java.io.InputStream;
import java.util.Properties;

@NoArgsConstructor
public class Bootstrap {

    @NotNull private final IProjectService projectService = new ProjectService(this);

    @NotNull private final ITaskService taskService = new TaskService(this);

    @NotNull private final IUserService userService = new UserService(this);

    @NotNull private final ISessionService sessionService = new SessionService(this);

    @NotNull private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(sessionService, projectService);

    @NotNull private final ITaskEndpoint taskEndpoint = new TaskEndpoint(sessionService, taskService);

    @NotNull private final IUserEndpoint userEndpoint = new UserEndpoint(sessionService, userService);

    @NotNull private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService, userService);

    @Nullable private SqlSessionFactory sqlSessionFactory;

    @NotNull public SqlSessionFactory getSqlSessionFactory() {
        return sqlSessionFactory;
    }

    @NotNull
    private SqlSessionFactory initSqlSessionFactory() throws Exception {
        @NotNull final Properties properties = new Properties();
        @NotNull final InputStream is = Bootstrap.class.getResourceAsStream("/database.properties");
        properties.load(is);
        @Nullable final String login = properties.getProperty("db.login");
        @Nullable final String host = properties.getProperty("db.host");
        @Nullable final String password = properties.getProperty("db.password");
        @Nullable final String driver = properties.getProperty("jdbcDriver");

        @NotNull final DataSource dataSource = new PooledDataSource(driver, host, login, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    public void start() throws Exception {
        Endpoint.publish("http://localhost:8080/projectService?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/taskservice?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/userservice?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/sessionservice?wsdl", sessionEndpoint);
        sqlSessionFactory = initSqlSessionFactory();
        @NotNull final SessionCleaner sessionCleaner = new SessionCleaner();
        sessionCleaner.setSessionService(sessionService);
        sessionCleaner.setDaemon(true);
        sessionCleaner.start();
    }
}
