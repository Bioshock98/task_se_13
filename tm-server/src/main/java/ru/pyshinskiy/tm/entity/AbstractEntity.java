package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractEntity implements Serializable {

    @NotNull
    private static final long SerialVersionUID = 1L;

    @Nullable
    private String id = UUID.randomUUID().toString();
}
