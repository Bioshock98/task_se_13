package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.enumerated.Status;

import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractWBS extends AbstractEntity {

    @Nullable
    private String userId;

    @NotNull
    private Date createTime = new Date(System.currentTimeMillis());

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Status status = Status.PLANNED;

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;
}
