package ru.pyshinskiy.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.task.ITaskRepository;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.entity.Task;

import java.sql.SQLException;
import java.util.List;

public final class TaskService extends AbstractWBSService<Task> implements ITaskService {

    @NotNull private final Bootstrap bootstrap;

    public TaskService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("task id is empty or null");
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findOneByUserId(userId, id);
    }

    @Override
    @NotNull
    public List<Task> findAll(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("task id is empty or null");
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeByUserId(userId, id);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeAllByUserId(userId);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    @NotNull
    public List<Task> findByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        if(name == null || name.isEmpty()) throw new Exception("name is empty or null");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findByName(userId, name);
    }

    @Override
    @NotNull
    public List<Task> findByDescription(@Nullable final String userId, @Nullable final String description) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        if(description == null || description.isEmpty()) throw new Exception("description is empty or null");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findByDescription(userId, description);
    }

    @Override
    @NotNull
    public List<Task> sortByCreateTime(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.sortByCreateTime(userId, direction);
    }

    @Override
    public @NotNull List<Task> sortByStartDate(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.sortByStartDate(userId, direction);
    }

    @Override
    @NotNull
    public List<Task> sortByFinishDate(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.sortByFinishDate(userId, direction);
    }

    @Override
    @NotNull
    public List<Task> sortByStatus(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.sortByStatus(userId, direction);
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("task id is empty or null");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findOne(id);
    }

    @Override
    @NotNull
    public List<Task> findAll() throws Exception {
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findAll();
    }

    @Override
    public void persist(@Nullable final Task task) throws Exception {
        if(task == null) throw new Exception("invalid task");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.persist(task);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void merge(@Nullable final Task task) throws Exception {
        if(task == null) throw new Exception("invalid task");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.merge(task);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid task id");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.remove(id);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeAll();
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if(userId == null || projectId == null) throw new Exception("one of the parameters passed is zero");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findAllByProjectId(userId, projectId);
    }
}
