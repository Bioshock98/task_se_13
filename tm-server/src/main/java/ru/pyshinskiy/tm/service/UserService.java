package ru.pyshinskiy.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.user.IUserRepository;
import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull private final Bootstrap bootstrap;

    public UserService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        return userRepository.findOne(id);
    }

    @Override
    @NotNull
    public List<User> findAll() throws Exception {
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        return userRepository.findAll();
    }

    @Override
    public void persist(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("invalid user");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.persist(user);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void merge(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("invalid user");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.merge(user);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid user id");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.remove(id);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.removeAll();
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    @Nullable
    public User getUserByLogin(@Nullable final String login) throws Exception {
        if(login == null) return null;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        return userRepository.getUserByLogin(login);
    }
}
