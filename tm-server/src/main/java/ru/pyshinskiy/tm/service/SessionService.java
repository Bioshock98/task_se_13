package ru.pyshinskiy.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.session.ISessionRepository;
import ru.pyshinskiy.tm.api.session.ISessionService;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.entity.Session;

import java.sql.SQLException;
import java.util.List;

public final class SessionService extends AbstractService<Session>  implements ISessionService {

    @NotNull private final Bootstrap bootstrap;

    public SessionService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    @Nullable
    public Session findOne(@Nullable final String userId, @Nullable final String id) throws Exception{
        if(userId == null) throw new Exception("invalid user id");
        if(id == null) throw new Exception("invalid session id");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        return sessionRepository.findOneByUserId(userId, id);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(userId == null) throw new Exception("invalid user id");
        if(id == null) throw new Exception("invalid session id");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.removeByUserId(userId, id);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid session id");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        return sessionRepository.findOne(id);
    }

    @Override
    public @NotNull List<Session> findAll() throws Exception {
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        return sessionRepository.findAll();
    }

    @Override
    public void persist(@Nullable final Session session) throws Exception {
        if(session == null) throw new Exception("invalid session");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.persist(session);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void merge(@Nullable final Session session) throws Exception {
        if(session == null) throw new Exception("invalid session");
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.merge(session);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.remove(id);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.removeAll();
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }
}
