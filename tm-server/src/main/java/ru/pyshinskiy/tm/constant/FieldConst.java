package ru.pyshinskiy.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class FieldConst {

    @NotNull public static final String ID = "id";
    @NotNull public static final String SIGNATURE = "signature";
    @NotNull public static final String ROLE = "role";
    @NotNull public static final String CREATE_TIME = "createTime";
    @NotNull public static final String USER_ID = "userId";
    @NotNull public static final String NAME = "name";
    @NotNull public static final String DESCRIPTION = "description";
    @NotNull public static final String START_DATE = "startDate";
    @NotNull public static final String FINISH_DATE = "finishDate";
    @NotNull public static final String STATUS = "status";
    @NotNull public static final String LOGIN = "login";
    @NotNull public static final String PASSWORD_HASH = "passwordHash";
    @NotNull public static final String TIMESTAMP = "timestamp";
}
